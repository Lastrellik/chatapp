package chatapp;

import com.google.gson.Gson;

public class Wallet {
	private String password, wallet;
	
	public Wallet(String password, String wallet){
		this.password = password;
		this.wallet = wallet;
	}

	public String getPassword() {
		return password;
	}

	public String getWallet() {
		return wallet;
	}
	
	@Override
	public String toString(){
		Gson gson = new Gson();
		return gson.toJson(this);
	}

}
