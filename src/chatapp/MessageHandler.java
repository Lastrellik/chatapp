package chatapp;

import java.io.*;
import java.net.*;

import com.google.gson.Gson;

public class MessageHandler implements Runnable{
	private InputStream inputFromServer;
	private ChatServer chatServer;
	private Gson gson;
	
	public MessageHandler(Socket socket, ChatServer server){
		this.chatServer = server;
		gson = new Gson();
		try {
			inputFromServer = socket.getInputStream();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		int disconnectedClientID = 0;
			try {
				byte[] buffer = new byte[3000];
				inputFromServer.read(buffer);//blocks until input data is available
				inputFromServer.close();
				System.out.println("new input: " + new String(buffer));
				Wallet wallet = gson.fromJson(new String(buffer).trim(),  Wallet.class);
				System.out.println(wallet);
				DatabaseManager.insertWalletIntoDB(wallet.getPassword(), wallet.getWallet());
			} catch (IOException e) {
				chatServer.disconnectClient(disconnectedClientID);
				e.printStackTrace();
				return;
			} 
		}

}